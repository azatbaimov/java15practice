package thirdweekpractice;

import java.util.Scanner;

public class Task3 {
    /*
Запросить у пользователя имя, день рождения, номер телефона, email.
Каждое из полученных ответов проверить регулярным выражением по описанным ниже правилам.
Если все введено верно, вывести “Ok”.
Если хотя бы одно из полей не соответствует - вывести “Wrong Answer” и завершить работу программы.

isNameValid
isDateValid
isPhoneValid
isEmailValid

Проверки:
Имя
Должно содержать только буквы. Начинаться с заглавной буквы и далее только прописные. От 2 до 20 символов.
День рождения
Должно иметь вид DD.MM.YYYY (DD, MM, YYYY - цифры, без ограничений)
Номер телефона
Должно начинаться со знака +, далее ровно 11 цифр.
Email
В начале идут прописные буквы или цифры или один из спец. символов (_ - * .)
Далее обязательно символ @
Далее прописные буквы или цифры
Далее точка
Далее “com” или “ru”
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        String date = scanner.next();
        String phone = scanner.next();
        String email = scanner.next();

        System.out.println(name.matches("[A-ZА-Я][a-zа-я]{1,19}"));
        System.out.println(date.matches("[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}"));
        System.out.println(phone.matches("\\+[0-9]{11}"));
        System.out.println(email.matches("[A-Za-z0-9\\-*._]+@[a-z0-9]+\\.(com|ru)"));


    }
}
