package thirdweekpractice;

import java.util.Scanner;

public class Task1 {
    /*
    Проверить цвет в hex
    Проверить, является ли
    введенная строка корректным hex номером цвета.
    Корректная строка состоит из
    7 символов, первый символ “#”, далее цифры или
    буквы от A до F (заглавные или прописные)
    Если строка является корректным hex номером цвета,
    то вывести true, иначе false.

    Вход        Выход
    #00AA12     true
    00FFFF      false
    #00FA       false
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String colour = scanner.next();
        System.out.println(colour.matches("#[0-9A-Fa-f]{6}"));
    }
}
