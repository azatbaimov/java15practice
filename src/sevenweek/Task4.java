package sevenweek;

import java.util.Scanner;

/*
Найти максимум из чисел последовательности

Дана последовательность натуральных чисел (одно число в строке),
завершающаяся числом 0.
Определите наибольшее значение числа в этой последовательности
 */
public class Task4 {
    public static void main(String[] args) {
        System.out.println(max());
    }

    public static int max() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n == 0) {
            return 0;
        } else {
            return Math.max(n, max());
        }
    }
}
