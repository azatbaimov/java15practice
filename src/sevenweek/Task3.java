package sevenweek;

import java.util.Scanner;

public class Task3 {
    /*
    Вы поднимаетесь по лестнице.
    Каждый раз вам нужно пройти n ступенек.
    За один раз вы можете подняться либо на одну
    либо на две ступени.
    Сколькими разными способами можно подняться по лестнице?
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(climb(n));
    }
    /*
    climb
    climb(1) = 1
    climb(2) = 1 + 1 или 2 2 способа
    climb(3) = climb(2) + climb(1) = 3
    climb(4) = climb(3) + climb(2) = 5
    climb(10) = climb(9) + climb(8)
    climb(n) = climb(n - 2) + climb(n - 1)
     */
    public static int climb(int n) {
        // Базовые случаи
        if (n == 1) {
            return 1;
        }

        if (n == 2) {
            return 2;
        }

        // Рекурсивный случай
        return climb(n - 2) + climb(n - 1);
    }
}
