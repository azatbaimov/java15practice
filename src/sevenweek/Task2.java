package sevenweek;

import java.util.Scanner;

/*
Дано число n - необходимо просуммировать
все цифры в числе n
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(sum(n));
    }
    public static int sum(int n) {
        // базовый случай
        if (n < 10) {
            return n;
        }

        // sum(345) = 5 + sum(34)
        // sum(34) = 4 + sum(3)
        // sum(3) = 3
        return n % 10 + sum(n / 10);
    }
}
