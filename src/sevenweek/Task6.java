package sevenweek;

public class Task6 {
    /*
    Дано слово, состоящее только из строчных латинских букв.
    Проверьте, является ли это слово палиндромом. Выведите YES или NO.
     */

    public static void main(String[] args) {
        System.out.println(palindrom("raar"));
    }
    // radar
    // rr
    //
    public static String palindrom(String s) {
        // Базовый случай
        if (s.length() == 1) {
            return "YES";
        } else {
            if (s.substring(0, 1).equals(s.substring(s.length() - 1, s.length()))) {
                // Базовый случай номер 2
                if (s.length() == 2) {
                    return "YES";
                }
                // Рекурсивный случай
                return palindrom(s.substring(1, s.length() - 1));
            } else {
                return "NO";
            }

        }
    }
}
