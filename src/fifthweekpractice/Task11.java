package fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

/*
   На вход подается число N — длина массива.
    Затем передается массив целых чисел из N элементов.
    Необходимо переставить элементы в полученном массиве в
    обратном порядке и вывести их на экран.
    Важно: нельзя использовать дополнительный массив.

    Вход                    Выход
    8                       2 123 38 9 75 38 2 10
    10 2 38 75 9 38 123 2
 */

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < n / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[n - 1 - i];
            arr[n - 1 - i] = temp;
        }

        System.out.println(Arrays.toString(arr));
    }
}
