package fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    /*
    На вход подается два отсортированных массива.
    Нужно создать отсортированный третий,
    состоящий из элементов первых двух.

    Вход            Выход
    5               1 1 2 3 4 6 7
    1 2 3 4 7
    2
    1 6

    4               10 20 20 20 123 125 272 400
    20 20 123 273
    4
    10 20 125 400
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n1 = scanner.nextInt();
        int[] arr1 = new int[n1];
        for (int i = 0; i < n1; i++) {
            arr1[i] = scanner.nextInt();
        }

        int n2 = scanner.nextInt();
        int[] arr2 = new int[n2];
        for (int i = 0; i < n2; i++) {
            arr2[i] = scanner.nextInt();
        }

        mergeArraysWithForLoop(arr1, arr2);
        mergeTwoArraysWithCopy(arr1, arr2);
        mergeTwoArrays(arr1, arr2);
    }

    /**
     * Сливает два отсортированных массива при помощи цикла for
     *
     * @param a1 - массив номер один
     * @param a2 - массив номер два
     */
    public static void mergeArraysWithForLoop(int[] a1, int[] a2) {
        int[] mergedArray = new int[a1.length + a2.length];
        int pos = 0;
        for (int elem : a1) {
            mergedArray[pos] = elem;
            pos++;
        }

        for (int elem : a2) {
            mergedArray[pos] = elem;
            pos++;
        }

        Arrays.sort(mergedArray); // 1 2 2 3 3
        System.out.println(Arrays.toString(mergedArray));
    }

    public static void mergeTwoArraysWithCopy(int[] a1, int[] a2){
        int[] mergedArray = new int[a1.length + a2.length];
        System.arraycopy(a1, 0, mergedArray, 0, a1.length);
        System.arraycopy(a2, 0, mergedArray, a1.length, a2.length);
        Arrays.sort(mergedArray);
        System.out.println(Arrays.toString(mergedArray));
    }

    public static void mergeTwoArrays(int[] a1, int[] a2){
        int[] mergedArray = new int[a1.length + a2.length];
        int i = 0, j = 0, k = 0;

        // 0 0 0 0
        // 1 2
        // 1 6
        // 1 1 2 0
        while (i < a1.length && j < a2.length) {
            if (a1[i] < a2[j]) {
                mergedArray[k++] = a1[i++];
                //mergedArray[k] = a1[i];
                //k++;
                //i++;
            } else {
                mergedArray[k++] = a2[j++];
            }
        }

        while (i < a1.length) {
            mergedArray[k++] = a1[i++];
        }

        while (j < a2.length) {
            mergedArray[k++] = a2[j++];
        }
        System.out.println(Arrays.toString(mergedArray));
    }
}
