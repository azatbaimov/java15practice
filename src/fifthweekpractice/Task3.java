package fifthweekpractice;

import java.util.Scanner;

public class Task3 {
    /*
        На вход подается число N — длина массива.
    Затем передается массив целых чисел длины N.
    Проверить, является ли он
    отсортированным массивом по убыванию.
    Если да, вывести true, иначе вывести false.

    Вход            Выход
    5               true
    5 4 3 2 1

    4               false
    20 20 11 13

    1               true
    43
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }

        // 20 20 11 13
        // 6 5 4 2 3 1
        // 5 4 3 2 1
        boolean flag = true;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] <= arr[i + 1]) {
                flag = false;
                break;
            }
        }

        System.out.println(flag);
    }
}
