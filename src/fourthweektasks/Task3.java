package fourthweektasks;

import java.util.Scanner;

public class Task3 {
    /*
    Дано число m < 13 и n < 7.
    Вывести все степени (от 0 до n включительно)
    числа m с помощью цикла.
    Входные данные
    3 6
    Выходные данные
    1
    3
    9
    27
    81
    243
    729

    Входные данные
    12 3
    Выходные данные
    1
    12
    144
    1728
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        for (int i = 0; i <= n; i++) {
            System.out.println((int)Math.pow(m, i));
        }
    }
}
