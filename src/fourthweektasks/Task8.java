package fourthweektasks;

import java.util.Scanner;

public class Task8 {
    /*
    Запросить у пользователя число строго больше 0.
    Повторять ввод до тех пор, пока не будет введено
    корректное число.
    Вывести “Отлично”, когда ввели корректное число.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число больше нуля");
        int n = scanner.nextInt();
        while (n <= 0){
            System.out.println("Неверно! Введите число больше нуля");
            n = scanner.nextInt();
        }
        System.out.println("Отлично!");
    }
}
