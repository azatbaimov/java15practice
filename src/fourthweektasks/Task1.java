package fourthweektasks;

import java.util.Scanner;

public class Task1 {
    /*
    Дано число n < 13.
    Найти факториал числа n (n! = 1 * 2 * 3 * …* (n - 1) * n)

    Входные данные Выходные данные
    12              479001600
    7               5040
    1               1
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial = factorial * i;
            System.out.println(i + " " + factorial);
        }
        System.out.println("Факториал числа " + factorial);
    }
}
