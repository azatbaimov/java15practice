package fourthweektasks;

import java.util.Scanner;

public class Task9 {
    /*
    Пользователь вводит символ.
    Повторять ввод до тех пор, пока не будет введена цифра.
    Вывести “Отлично”, когда ввели цифру.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char c;
        do {
            System.out.println("Введите цифру!");
            c = scanner.next().charAt(0);
        } while (c < '0' || c > '9');
        System.out.println("Отлично!");
    }
}
