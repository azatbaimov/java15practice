package fourthweektasks;

import java.util.Scanner;

public class Task7 {
    /*
    На вход подается число n и последовательность целых чисел длины n.
    Вывести два максимальных числа в этой последовательности
    без использования массивов.

    Входные данные
    10
    8 79 61 94 59 4 34 6 77 10

    Выходные данные
    94 79

    Входные данные
    5
    3 38 8 38 12

    Выходные данные
    38 38
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int firstNum = scanner.nextInt();
        int secondNum = scanner.nextInt();

        int max = Math.max(firstNum, secondNum);
        int secondMax = Math.min(firstNum, secondNum);

        for (int i = 2; i < n; i++) {
            int k = scanner.nextInt();
            if (k > max) {
                if (max > secondMax) {
                    secondMax = max;
                }
                max = k;
            } else if (k > secondMax) {
                secondMax = k;
            }
        }

        System.out.println(max);
        System.out.println(secondMax);
    }
}
