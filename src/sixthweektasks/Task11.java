package sixthweektasks;

import java.util.Scanner;

public class Task11 {
    /*
    Реализовать алгоритм Евклида рекурсивно.
    Алгоритм Евклида
    находит наибольший общий делитель двух чисел.
    Формула НОД: НОД (a, b) = НОД (b, с),
    где с — остаток от деления a на b


    Алгоритм Евклида заключается в следующем:
    если большее из двух чисел делится на меньшее —
    наименьшее число и будет их наибольшим общим делителем.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(gcd(a, b));
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
}
