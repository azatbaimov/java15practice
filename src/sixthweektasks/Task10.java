package sixthweektasks;

import java.util.Scanner;

public class Task10 {
    /*
    На вход подается строка.
    Развернуть строку рекурсивно.

    Вход
    abcdef

    Выход
    fedcba
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        System.out.println(reverseString(s));
    }

    public static String reverseString(String input) {
        if (input.isEmpty()) {
            return input;
        }
        return reverseString(input.substring(1)) + input.charAt(0);
    }
    // abcdef
    //'' + f + e + d + c + b + a
}
