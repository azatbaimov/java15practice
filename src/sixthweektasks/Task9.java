package sixthweektasks;

import java.util.Scanner;

public class Task9 {
    /*
    На вход подается натуральное число N.
    Необходимо проверить, является ли оно
    степенью двойки (решить через рекурсию).
    Вывести true, если является и false иначе.

    Вход
    4
    Выход
    true

    Вход
    5
    Выход
    false
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(checkPowerOfTwo(n));

    }

    public static boolean checkPowerOfTwo(int n) {
        if (n == 2) {
            return true;
        }

        if (n % 2 != 0) {
            return false;
        } else {
            return checkPowerOfTwo(n / 2);
        }
    }
}
