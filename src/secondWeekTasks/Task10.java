package secondWeekTasks;

import java.util.Scanner;

public class Task10 {
    /*
    Дана последовательность символов,
    конкатенировать их в одну строку и вывести эту строку,
    исключая цифры
    Входные данные
    H e 1 2 3 l l 4 o
    Выходные данные
    Hello

    Входные данные
    1 2 3 T 4 5 e a m
    Выходные данные
    Team
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a1 = scanner.next();
        String a2 = scanner.next();
        String a3 = scanner.next();
        String a4 = scanner.next();
        String a5 = scanner.next();
        String a6 = scanner.next();
        String a7 = scanner.next();
        String a8 = scanner.next();
        String a9 = scanner.next();

        String result = "";
        if ((a1.charAt(0) >= 'a' && a1.charAt(0) <= 'z') || (a1.charAt(0) >= 'A' && a1.charAt(0) <= 'Z')) {
            result += a1;
        }

        if ((a2.charAt(0) >= 'a' && a2.charAt(0) <= 'z') || (a2.charAt(0) >= 'A' && a2.charAt(0) <= 'Z')) {
            result += a2;
        }

        if ((a3.charAt(0) >= 'a' && a3.charAt(0) <= 'z') || (a3.charAt(0) >= 'A' && a3.charAt(0) <= 'Z')) {
            result += a3;
        }

        if ((a4.charAt(0) >= 'a' && a4.charAt(0) <= 'z') || (a4.charAt(0) >= 'A' && a4.charAt(0) <= 'Z')) {
            result += a4;
        }

        if ((a5.charAt(0) >= 'a' && a5.charAt(0) <= 'z') || (a5.charAt(0) >= 'A' && a5.charAt(0) <= 'Z')) {
            result += a5;
        }

        if ((a6.charAt(0) >= 'a' && a6.charAt(0) <= 'z') || (a6.charAt(0) >= 'A' && a6.charAt(0) <= 'Z')) {
            result += a6;
        }

        if ((a7.charAt(0) >= 'a' && a7.charAt(0) <= 'z') || (a7.charAt(0) >= 'A' && a7.charAt(0) <= 'Z')) {
            result += a7;
        }

        if ((a8.charAt(0) >= 'a' && a8.charAt(0) <= 'z') || (a8.charAt(0) >= 'A' && a8.charAt(0) <= 'Z')) {
            result += a8;
        }

        if ((a9.charAt(0) >= 'a' && a9.charAt(0) <= 'z') || (a9.charAt(0) >= 'A' && a9.charAt(0) <= 'Z')) {
            result += a9;
        }

        System.out.println(result);
    }
}
