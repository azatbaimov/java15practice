package secondWeekTasks;

import java.util.Scanner;

public class Task8 {
    /*
        Дан символ,
        поменять со строчного на заглавный или с
        заглавного на строчный
        Входные данные
        A
        Выходные данные
        a
        Входные данные
        b
        Выходные данные
        B
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String symbol = scanner.next();
        char c = symbol.charAt(0);

        if (c >= 'a' && c <= 'z') {
            // A - 65
            // a - 97
            // b - 98
            // B - 66
            System.out.println((char) (c - ('a' - 'A')));
        } else {
            System.out.println((char) (c + ('a' - 'A')));
        }
    }
}
