package secondWeekTasks;

import java.util.Scanner;

public class Task6 {
    /*
    Даны три числа a, b, c .
    Найти сумму двух чисел больших из них.
    Входные данные
    21 0 8
    Выходные данные
    29
    Входные данные
    60 9 6
    Выходные данные
    69
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("Входные данные");
        System.out.println(a + " " + b + " " + c);
        System.out.println(a + b + c - Math.min(Math.min(a, b), c));
        int sum = (a > b || a > c) ? a + Math.max(b, c) : b + c;
        System.out.println(sum);
    }
}
