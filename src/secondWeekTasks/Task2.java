package secondWeekTasks;

import java.util.Scanner;

public class Task2 {
    /*
    Дано число n. Нужно проверить четное ли оно.
    Входные данные
    4
    Выходные данные
	true
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n % 2 == 0) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        // Решение через тернарный оператор
        boolean isEven = (n % 2 == 0) ? true : false;
        System.out.println("Через тернарный оператор: " + isEven);
    }
}
