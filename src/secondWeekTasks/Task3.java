package secondWeekTasks;

import java.util.Scanner;

public class Task3 {
    /*
    Считать данные из консоли о
    типе номера отеля. 1 VIP, 2 Premium 3 Comfort
    Вывести цену номера
    VIP 125
    Premium 110
    Comfort 100
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите тип номера в отеле");
        int roomType = scanner.nextInt();

        switch (roomType) {
            case 1:
                System.out.println("Номер VIP");
                System.out.println("Цена номера " + 125);
                break;
            case 2:
                System.out.println("Номер Premium");
                System.out.println("Цена номера " + 110);
                break;
            case 3:
                System.out.println("Номер Comfort");
                System.out.println("Цена номера " + 100);
                break;
            default:
                System.out.println("Такой тип номера отсутствует");
                break;
        }

    }
}
