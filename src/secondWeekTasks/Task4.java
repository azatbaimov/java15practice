package secondWeekTasks;

import java.util.Scanner;

public class Task4 {
    /*
    Даны два числа a и b.
    Проверить утверждение,
    что хотя бы одно из них нечетное.
    Входные данные
    63 56
    Выходные данные
    true
    Входные данные
    67 69
    Выходные данные
    true
    Входные данные
    83 35
    Выходные данные
    true
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите два числа");
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        boolean aIsOdd = a % 2 == 1;
        boolean bIsOdd = b % 2 == 1;

        // Хотя бы одно из чисел нечетное
        System.out.println("Хотя бы одно нечетное");
        if (aIsOdd || bIsOdd) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        // Вывести true если только одно из чисел нечетное
        // Но не оба!
        /*
    A       B       A ^ B
    true    true    false
    true    false   true
    false   true    true
    false   false   false
         */
        System.out.println("Не оба!");
        if (aIsOdd ^ bIsOdd) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        System.out.println("Без шляпки!");
        if ((aIsOdd && !bIsOdd) || (bIsOdd && !aIsOdd)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
