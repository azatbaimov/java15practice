package secondWeekTasks;

import java.util.Scanner;

public class Task7 {
    /*
    Реализовать System.out.println(),
    используя System.out.print()

    Входные данные
    Hello world

    Выходные данные
    Hello
    world
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.next();
        String second = scanner.next();

        System.out.print(first + "\n");
        System.out.print(second);
    }
}
