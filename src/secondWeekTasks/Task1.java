package secondWeekTasks;

import java.util.Scanner;

public class Task1 {
    /*
    Дано четырехзначное число.
    Проверить является ли оно палиндромом.

    Входные данные
    1881
    Выходные данные
    true
    Входные данные
    5081
    Выходные данные
    false
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        int start = input / 1000;
        int end = input % 10;

        if (start != end) {
            System.out.println(false);
        } else {
            start = (input / 100) % 10;
            end = (input % 100) / 10;

            if (start != end) {
                System.out.println(false);
            } else {
                System.out.println(true);
            }
        }

    }
}
