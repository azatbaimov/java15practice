package firstWeekTasks;

import java.util.Scanner;

public class Task1 {
    /*
    Даны числа a, b, c.
    Перенести значения из a в b,
    из b в c, из c в a.

    Входные данные
    3 2 1
    Выходные данные
    a = 1, b = 3,  c=2

    a = 3
    b = 2
    c = 1

    a --> b
    b --> c
    c --> a
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("a = " + a + " b = " + b + " c = " + c);
        int temp = a;
        a = c;
        c = b;
        b = temp;

        System.out.println("a = " + a + " b = " + b + " c = " + c);
    }
}
