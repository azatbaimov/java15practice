package firstWeekTasks;

import java.util.Scanner;

public class Task4 {
    /*
    Дана площадь круга,
    нужно найти диаметр окружности и длину окружности.
    Напоминаем, что:
    S  = PI * D^2 / 4, где PI ~ 3.14159265359

    D = (S * 4 / PI)^(1/2)
    L = 2 * PI * R = PI * D

    Входные данные:
    91
    Diameter: 10.764051215546116 length: 33.81626422162396
    Входные данные:
    4
     */
    public static void main(String[] args) {
        System.out.println("Введите площадь круга");
        Scanner scanner = new Scanner(System.in);
        double square = scanner.nextDouble();

        double diameter = Math.sqrt(square * 4 / Math.PI);
        double length = Math.PI * diameter;

        System.out.println("Diameter: " + diameter + " length: " + length);
    }
}
